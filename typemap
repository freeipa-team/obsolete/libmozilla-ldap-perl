#############################################################################
# $Id: typemap,v 1.5.2.6 2007/06/14 09:21:15 gerv%gerv.net Exp $
#
# ***** BEGIN LICENSE BLOCK *****
# Version: MPL 1.1/GPL 2.0/LGPL 2.1
#
# The contents of this file are subject to the Mozilla Public License Version
# 1.1 (the "License"); you may not use this file except in compliance with
# the License. You may obtain a copy of the License at
# http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS IS" basis,
# WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
# for the specific language governing rights and limitations under the
# License.
#
# The Original Code is PerLDAP.
#
# The Initial Developer of the Original Code is
# Netscape Communications Corporation.
# Portions created by the Initial Developer are Copyright (C) 2001
# the Initial Developer. All Rights Reserved.
#
# Contributor(s):
#   Clayton Donley
#   Leif Hedstrom <leif@perldap.org>
#   Kevin McCarthy
#
# Alternatively, the contents of this file may be used under the terms of
# either the GNU General Public License Version 2 or later (the "GPL"), or
# the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
# in which case the provisions of the GPL or the LGPL are applicable instead
# of those above. If you wish to allow use of your version of this file only
# under the terms of either the GPL or the LGPL, and not to allow others to
# use your version of this file under the terms of the MPL, indicate your
# decision by deleting the provisions above and replace them with the notice
# and other provisions required by the GPL or the LGPL. If you do not delete
# the provisions above, a recipient may use your version of this file under
# the terms of any one of the MPL, the GPL or the LGPL.
#
# ***** END LICENSE BLOCK *****

# DESCRIPTION
#    Typemap to declare XSUB data types.

const int		T_IV
char *			T_PV
const char *		T_PV
char **			T_charptrptr
void *			T_PTR
LDAP *			T_PTR
LDAPMessage *		T_PTR
LDAPMessage **		T_PTR
BerElement *		T_PTR
LDAPControl *		T_PTR
LDAPVersion *		T_PTR
struct berval 		T_berval
struct berval *		T_bervalptr
struct berval **	T_bervalptrptr
FriendlyMap *		T_PTR
LDAPsortkey **		T_PTR
LDAPVirtualList *	T_PTR
LDAPURLDesc *		T_PTR
LDAPControl **		T_PTR
LDAPFiltDesc *		T_PTR
LDAPFiltInfo *		T_PTR
LDAPMemCache *		T_PTR
LDAPMemCache **		T_PTR
struct ldap_thread_fns *	T_PTR
LDAPMod **		T_PTR
LDAP_CMP_CALLBACK *	T_PTR
LDAP_REBINDPROC_CALLBACK *	T_PTR
struct timeval		T_timeval
my_chgtype_t    T_IV
my_chgnum_t     T_IV
my_result_t     T_IV
my_vlvint_t     T_IV

#########
INPUT
T_timeval
	$var.tv_sec = (long)atof((char *)SvPV($arg,PL_na));
	$var.tv_usec = 0
T_berval
	$var.bv_val = (char *)SvPV($arg,PL_na);
	$var.bv_len = PL_na
T_charptrptr
        $var = (char **)avref2charptrptr($arg)
T_bervalptr
    $var = NULL
T_bervalptrptr
	$var = (struct berval **)avref2berptrptr($arg)
OUTPUT
T_charptrptr
	$arg = charptrptr2avref($var);
T_bervalptr
    if ($var) {
        sv_setpvn($arg,$var->bv_val,$var->bv_len);
        ber_bvfree($var);
    }
T_bervalptrptr
	$arg = berptrptr2avref((struct berval **)$var);
